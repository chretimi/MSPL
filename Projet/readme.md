# Jeu de données

Le jeu de données est disponible [ici](https://www.kaggle.com/NUFORC/ufo-sightings), il contient plus de 80 000 entrées, chaque entrée représente une déclaration d'une personne ayant vu un OVNI.

## Contenu

Le jeu est disponible en 2 versions, une complète et l'autre "nettoyé".
La version nettoyé exclut les entrées sans localisation ou sans heure indiquée.

## Références

Les données proviennent et sont nettoyée par le NUFORC data par Sigmond Axel [github](https://github.com/planetsig/ufo-reports).